
public class RATCLASS {
	private int Rat_usage;
	private int Rat_Capacity;
	private double  RAT_Price;
	private double RAT_security;
	private String RAT_sec;
	private int RAT_data;
	private int RAT_Threshold;
	private int RAT_blocked;
	private int RAT_dropped;
	private int rat_new;
	private int rathandoff;
	private int admitted;
	private int ID;
	
	private double maxprice = 5.0;
	
	public RATCLASS(int Rat_Capacity,int RAT_Threshold,double  RAT_Price,int RAT_data,String RAT_sec){
		this.Rat_Capacity=Rat_Capacity;
		this.RAT_data=RAT_data;
		this.RAT_sec=RAT_sec;
		this.RAT_Price=RAT_Price;
		this.RAT_Threshold=RAT_Threshold;
	}
	
	public void setusage(){
		 this.Rat_usage+=1;
	}
	
	public void resetusage(){
		if (!(Rat_usage <=0)){
			this.Rat_usage-=1;
		}
	}
	
		public int getData (){
			return this.RAT_data;
			
		}
		
		public void  setData (int data){
			this.RAT_data= data;
			
		}
		
		public String getsec(){
			return this.RAT_sec;
			
		}
		public double getsecurity(){
			return this.RAT_security;
			
		}
		
		public double getprice(){
			return this.RAT_Price;
			
		}
		
		public void resetprice(int price){
			 RAT_Price=price;
		}
		
		public int getthreshold (){
			return this.RAT_Threshold;
			
		}
		
		public int getcapacity(){
			return this.Rat_Capacity;
			
		}
		
		public int getratusage(){
			return this.Rat_usage;
			
		}
		
		public int getratnew (){
			return this.rat_new;
			
		}
		
		public int gethandoff(){
			return this.rathandoff;
			
		}
		
		public void setsecurity(double number){
			this.RAT_security=number;
			
		}
		
		public void setprice(double average_load){
			
			// check congestion in the rat and then updtae the price
			if (Rat_usage>average_load || Rat_usage>=Rat_Capacity){
				//double newprice = RAT_Price+(0.75*(Rat_usage-average_load));
				double newprice = RAT_Price+0.1;
				if (newprice < maxprice){
					this.RAT_Price=newprice;
					
					// for static comment the top line and the run then uncomment the next lines
					//this.RAT_Price=0.5;
				}
				else {
					this.RAT_Price=RAT_Price;
					// for static comment the top line and the run then uncomment the next lines
					//this.RAT_Price=0.5;
				}
			}
			else if ((Rat_usage<average_load)){
				
				double newprice = RAT_Price-0.2;
				if (newprice >= 0.5){
					this.RAT_Price=newprice;
					
				}
				else {
					
					this.RAT_Price=RAT_Price;
					
				}
				
			}
			
			}
		
		public void setpricespecial(double average_load){
			
			// check congestion in the rat and then updtae the price
			if (RAT_blocked>average_load){
				//double newprice = RAT_Price+(0.75*(Rat_usage-average_load));
				double newprice = RAT_Price+0.2;
				if (newprice < maxprice){
					this.RAT_Price=newprice;
				}
				else {
					this.RAT_Price=RAT_Price;
				}
			}
			else{
				
				double newprice = RAT_Price-0.2;
				if (newprice >= 0.5){
					this.RAT_Price=newprice;
				}
				else {
					this.RAT_Price=RAT_Price;
				}
				
			}
			
			}
		
		public void setnew(){
			
			this.rat_new +=1;
		}
		
		
		public void sethandoff(){
			
			this.rathandoff+=1;
		}
		
		public void setblocked(){
		this.RAT_blocked+=1;
		}
		
		public void setadmitted(){
			this.admitted+=1;
		}
			
		public int  getblocked(){
			return this.RAT_blocked;
			}
		
		public int  setdropped(){
			return this.RAT_dropped+=1;
			}
		
		public int  getdropped(){
			return this.RAT_dropped;
			}
		
		
		public int getadmitted(){
			return this.admitted;
		}
		
		public void resetnew(){
			
			if (!(this.rat_new <=0)){
				this.rat_new-=1;
			}
				
			}
	
}
