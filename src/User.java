import java.util.ArrayList;

class Random {
	static double exponential(double mean) {
		return -mean * Math.log(Math.random());
	}

	static boolean bernoulli(double p) {
		return Math.random() < p;
	}

}

class call extends Event {
	double average = 0;
	double averageblocked = 0;
	int temp = 0;
	//public int tempo;
	int variable;
	
	call(double time, RATCLASS[] Array, pricing pricing,
			ArrayList<Integer> Rat1User) {
		this.time = time;
		this.Array = Array;
		this.pricing = pricing;
		this.Rat1User = Rat1User;
	}
	

	call(double time, RATCLASS[] Array, pricing pricing,
			ArrayList<Integer> Rat1User,int tempo) {
		this.time = time;
		this.Array = Array;
		this.pricing = pricing;
		this.Rat1User = Rat1User;
		this.tempo = tempo;
		variable = tempo;
	}

	void execute(AbstractSimulator simulator) {

		if (Array[0].getadmitted() >= pricing.getaverage()) {
			// if (Array[0].getratusage()>15){
			temp = 1;
			// System.out.println("here 1");
		}

		if (Array[1].getadmitted() >= pricing.getaverage()) {
			// if (Array[1].getratusage()>15){
			temp = 2;
			// System.out.println("here 2");
		}

		if (Array[2].getadmitted() >= pricing.getaverage()) {
			// if (Array[2].getratusage()>1
			temp = 3;
			// System.out.println("here 3");
		}

		pricing.NormaliseDecionmatrix();

		int ratselected = pricing.rat_selection(temp);

		//System.out.println("here " + tempo);

		//int tempo = (Math.random() <= 0.5) ? 1 : 2;

		if (ratselected == 1) {
			if (tempo == 1) {
				if ((Array[0].getratusage() < Array[0].getcapacity())
						& (Array[0].getratnew() < Array[0].getthreshold())) {
					Array[0].setnew();
					Array[0].setusage();
					// double average =
					// (Array[0].getratusage()+Array[1].getratusage()+Array[2].getratusage())/3;
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2].getratusage()) / 3;
					Array[0].setprice(pricing.getaverage());
					Array[0].setadmitted();
					pricing.setaverage(average);
					// Array[0]User.add(user);
					// System.out.println("new call admitted into Array[0]");
					// System.out.println(
				} else {
					Array[0].setblocked();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					averageblocked = (Array[0].getblocked()
							+ Array[1].getblocked() + Array[2].getblocked()) / 3;
					pricing.setaverageblocked(averageblocked);
					Array[0].setpricespecial(pricing.getaverageblocked());
					Array[0].setprice(pricing.getaverage());

				}
			} else if ((tempo == 2)) {
				if ((Array[0].getratusage() < Array[0].getcapacity())) {
					Array[0].sethandoff();
					Array[0].setusage();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2].getratusage()) / 3;
					pricing.setaverage(average);
					Array[0].setprice(pricing.getaverage());
					Array[0].setadmitted();
					// Array[0]User.add(user);
					// System.out.println("handoff call admitted into Array[0]");

					// Array[0]User.add(user);

				} else {
					Array[0].setdropped();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[0].setprice(pricing.getaverage());

				}
			}

		}

		else if (ratselected == 2) {
			if (tempo == 1) {
				if ((Array[1].getratusage() < Array[1].getcapacity())
						& (Array[1].getratnew() < Array[1].getthreshold())) {
					Array[1].setnew();
					Array[1].setusage();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[1].setprice(pricing.getaverage());
					Array[1].setadmitted();
					// System.out.println("new call admitted into Array[1]");
					// Array[1]User.add(user);
				} else {
					Array[1].setblocked();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					averageblocked = (Array[0].getblocked()
							+ Array[1].getblocked() + Array[2].getblocked()) / 3;
					pricing.setaverageblocked(averageblocked);
					Array[1].setprice(pricing.getaverage());
					Array[1].setpricespecial(averageblocked);
				}
			} else if ((tempo == 2)) {
				if ((Array[1].getratusage() < Array[1].getcapacity())) {
					Array[1].sethandoff();
					Array[1].setusage();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[1].setprice(pricing.getaverage());
					Array[1].setadmitted();
					// Array[1]User.add(user);
					// System.out.println("handoff call admitted into Array[1]");
					// Array[1]User.add(user);
				} else {
					Array[1].setdropped();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[1].setprice(pricing.getaverage());
				}
			}
		}

		if (ratselected == 3) {
			// System.out.println("i am here]");
			if (tempo == 1) {
				if ((Array[2].getratusage() < Array[2].getcapacity())
						& (Array[2].getratnew() < Array[2].getthreshold())) {
					Array[2].setnew();
					Array[2].setusage();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[2].setprice(pricing.getaverage());
					Array[2].setadmitted();
					// System.out.println("new call admitted into Array[2]");
					// Array[2]User.add(user);
				} else {
					Array[2].setblocked();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					averageblocked = (Array[0].getblocked()
							+ Array[1].getblocked() + Array[2].getblocked()) / 3;
					pricing.setaverageblocked(averageblocked);
					pricing.setaverage(average);
					Array[2].setprice(pricing.getaverage());
					Array[2].setpricespecial(averageblocked);
				}
			} else if ((tempo == 2)) {
				if ((Array[2].getratusage() < Array[2].getcapacity())) {
					Array[2].sethandoff();
					Array[2].setusage();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[2].setprice(pricing.getaverage());
					Array[2].setadmitted();
					// Array[2]User.add(user);
					// System.out.println("handoff call admitted into Array[2]");
					// Array[2]User.add(user);
				} else {
					Array[2].setdropped();
					average = (Array[0].getratusage() + Array[1].getratusage() + Array[2]
							.getratusage()) / 3;
					pricing.setaverage(average);
					Array[2].setprice(pricing.getaverage());
				}
			}

		}

		// System.out.println(average);
	}
}

class PricingSimulator extends Simulator {

	public static void main(String[] args) {
		// new PricingSimulator().start();

		PricingSimulator sim = new PricingSimulator();

		sim.start();
		if (sim.now() == 25) {
			// System.out.println("hereeeeee");
			sim.stop();
		}

	}

	void start() {
		// adding my staff from here
		RATCLASS[] Array = new RATCLASS[3];

		// double averageblocked;
		// double averagedropped;

		// int temp=0;
		RATCLASS rat1 = new RATCLASS(40, 25, 0.5, 32, "high");
		RATCLASS rat2 = new RATCLASS(40, 25, 0.5, 64, "medium");
		RATCLASS rat3 = new RATCLASS(40, 25, 0.5, 128, "low");

		ArrayList<Integer> Rat1User = new ArrayList<Integer>();

		pricing pricing = new pricing();// this could be the issue
		pricing.fuzzy(rat1, rat2, rat3);
		int new_handoff = 1;
		// int tempo =1;

		pricing.NormaliseDecionmatrix();

		Array[0] = rat1;
		Array[1] = rat2;
		Array[2] = rat3;
		// end adding staff
		events = new ListQueue();

		for (int i = 0; i < 100; i++) {
			
			if (i == 0) {
				int tempo = (Math.random() <= 0.5) ? 1 : 2;
				insert(new call(Random.exponential(4.0), Array, pricing,
						Rat1User,tempo));
				
			} else {
				insert(new call(Random.exponential(4.0), Array, pricing,
						Rat1User));
			}

		}
		assignCallType();
		
		doAllEvents();

		// all a method that does the printing to hide the staff

		System.out.println("number of calls admitted in rat1: "+rat1.getadmitted());
		System.out.println("number of calls admitted in rat2: "+rat2.getadmitted());
		System.out.println("number of calls admitted in rat3: "+rat3.getadmitted());

		int total = rat1.getblocked() + rat2.getblocked() + rat3.getblocked();
		int tot = rat1.getdropped() + rat2.getdropped() + rat3.getdropped();

		System.out.println();

		System.out.println("number of calls blocked in the network: " + total);
		
		//System.out.println("number of calls blocked in the network: " +rat3.getblocked() );

		System.out.println();

		System.out.println("number of calls dropped in the network: " + tot);
		//System.out.println("number of calls dropped in the network: " + rat3.getdropped());

		// System.out.println("number of calls blocked in rat1: "+rat1.getblocked());
		// System.out.println("number of calls blocked in rat2: "+rat2.getblocked());
		// System.out.println("number of calls blocked in rat3: "+rat3.getblocked());1

		// System.out.println();

		// System.out.println("number of calls dropped in rat1: "+rat1.getdropped());
		// System.out.println("number of calls dropped in rat2: "+rat2.getdropped());
		// System.out.println("number of calls dropped in rat3: "+rat3.getdropped());
		//
		// System.out.println();
		
	
				System.out.println();
		
				System.out.println("number of new calls in rat1: "+rat1.getratnew());
				System.out.println("number of new calls in rat2: "+rat2.getratnew());
				System.out.println("number of new calls in rat3: "+rat3.getratnew());
				
				
			    System.out.println();
				
				//System.out.println("number of handoff calls in rat1: "+rat1.gethandoff());
				//System.out.println("number of handoff calls in rat2: "+rat2.gethandoff());
				//System.out.println("number of handoff calls in rat3: "+rat3.gethandoff());
				
			
	}
}