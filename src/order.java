import java.util.ArrayList;
import java.util.Arrays;

abstract class Event extends AbstractEvent {
    double time;
    RATCLASS [] Array;
    pricing pricing;
    ArrayList<Integer> Rat1User;
    public int tempo=0;
    
    public boolean lessThan(Comparable y) {
        Event e = (Event) y;  // Will throw an exception if y is not an Event
        return this.time < e.time;
    }
}

class Simulator extends AbstractSimulator {
    double time;
    double now() {
        return time;
    }
    
    public void stop(){
    	
    }
    
    void doAllEvents() {
        Event e;
        while ( (e= (Event) events.removeFirst()) != null) {
            time = e.time;
            e.execute(this);
        }
    }
}
