class ListQueue extends OrderedSet {
	java.util.Vector elements = new java.util.Vector();

	void insert(Comparable x) {

		int i = 0;
		while (i < elements.size()
				&& ((Comparable) elements.elementAt(i)).lessThan(x)) {
			i++;
		}

		/*Event e = (Event) x;
		if (i != 0) {
			Event prev = (Event) elements.get(i - 1);
			switch (prev.tempo) {
			case 1:
				e.tempo = 2;
				break;
			case 2:
				e.tempo = 1;
				break;

			}
		}*/
		
		

		elements.insertElementAt(x, i);
		
		
		
		
	}
	
	void assignType(){
		int type = (Math.random() <= 0.5) ? 1 : 2;
		
		for( int i = 0; i <elements.size() ; i++){
			Event event = (Event) elements.get(i);
			
			switch (type) {
			case 1:
				event.tempo = 2;
				type = 2;
				break;
			case 2:
				event.tempo = 1;
				type = 1;
				break;

			}
			
		}
	}
	
	
	

	Comparable removeFirst() {
		if (elements.size() == 0)
			return null;
		Comparable x = (Comparable) elements.firstElement();
		elements.removeElementAt(0);
		return x;
	}

	Comparable remove(Comparable x) {
		for (int i = 0; i < elements.size(); i++) {
			if (elements.elementAt(i).equals(x)) {
				Object y = elements.elementAt(i);
				elements.removeElementAt(i);
				return (Comparable) y;
			}
		}
		return null;
	}

	public int size() {
		return elements.size();
	}

}