import java.util.ArrayList;
import java.util.Arrays;

public class pricing {
	
	private RATCLASS[] RatArray = new RATCLASS [3];
	
	private int [] RatScores = new int [3];
	private int [] data =new int [3] ;
	private double [] security =new double [3] ;
	private double [] price=new double [3] ;
	private int [] weights =new int [3] ;
	private double [] normaweights =new double[3] ;
	
	private double average =0;
	private double averageblocked=0;
	
	private double normData;
	private double normPrice;	
	private double [] normDataArray=new double  [3];	
	public double  [] normPriceArray=new double [3] ;
	double sum=0;

	private double Rat1_score =0;
	private double Rat2_score =0;
	private double Rat3_score =0;
	
	private int SelectedRat=0;
	public pricing(){
		
	}
	
	
	public void setaverage(double  average){
		this.average =average;
	}
	
	public double getaverage(){
		return this.average;
	}
	
	public void setaverageblocked(double averageblocked){
		this.averageblocked =averageblocked;
	}
	
	public double getaverageblocked(){
		return this.averageblocked;
	}
	
	
	
	
	public void fuzzy(RATCLASS rat1,RATCLASS rat2 ,RATCLASS rat3 ){
		RatArray[0]=rat1;
		RatArray[1]=rat2;
		RatArray[2]=rat3;
		
		for (int i=0;i<RatArray.length;i++ ){
			if (RatArray[i].getsec().equalsIgnoreCase("high")){// scaled down the effect of security
				RatArray[i].setsecurity(0.5*0.5);
			}
			else if(RatArray[i].getsec().equalsIgnoreCase("medium")){// scaled down the effect of security
				RatArray[i].setsecurity(0.3333333333*0.5);
			}
			else 
				RatArray[i].setsecurity(0.166666667*0.5);
			}
	}
	
	public void NormaliseDecionmatrix(){
		for	(int j=0;j<RatArray.length;j++){	
			data[j]	=RatArray[j].getData();	
		}	
		
		for	(int j=0;j<RatArray.length;j++){	
			price[j]=RatArray[j].getprice();	
		}
		
		for	(int j=0;j<RatArray.length;j++){	
			security[j]=RatArray[j].getsecurity();	
		}
			
		Arrays.sort(price);	
		Arrays.sort(data);	
		double maxData	=data[data.length-1];	
	    double  minPrice=price[0]; 
	    for	(int i=0;i<RatArray.length;i++){	
	    	normData=RatArray[i].getData()/maxData;	
	    	normPrice=minPrice/RatArray[i].getprice();	
	    	normDataArray[i]=normData*0.5;	//// scaled the effect of data rate down 
	    	normPriceArray[i]=normPrice;	 	
	    } 
	 }
	
	public int rat_selection (int temp){
		
		for (int i=0;i<3;i++){
			weights[i]=(int)(Math.random()*10+1);
		}
		
		for (int i=0;i<3;i++){
			sum += weights[i];
		}
		
		for (int i=0;i<3;i++){
			normaweights[i]=weights[i]/sum;
		
		}
		//// rat score calculation 
		 Rat1_score= (normaweights[0]*normPriceArray[0])+ (normaweights[1]*normDataArray[0])+(normaweights[2]*security[0]);
		 Rat2_score= (normaweights[0]*normPriceArray[1])+ (normaweights[1]*normDataArray[1])+(normaweights[2]*security[1]);
		 Rat3_score= (normaweights[0]*normPriceArray[2])+ (normaweights[1]*normDataArray[2])+(normaweights[2]*security[2]);
		 
		 if (temp ==1){
			 Rat1_score=0;
		 }
		 else  if (temp ==2){
			 Rat2_score=0;
		 }
		 else if (temp ==3){
			 Rat3_score=0;
		 }
		//System.out.println( Rat1_score+" "+Rat2_score+" "+Rat3_score);
		 
		 if ((Rat1_score>=Rat2_score) & (Rat1_score >=Rat3_score)){
			 SelectedRat=1;
			 
		 }
		 
		 else if ((Rat2_score>=Rat1_score) & (Rat2_score >=Rat3_score)){
			 SelectedRat=2; 
		 }
		 
		 else if ((Rat3_score>=Rat1_score) & (Rat3_score >=Rat2_score)){
			 SelectedRat=3;
		 }
		 
		
		 
		
		 //System.out.println( Rat1_score);
		// System.out.println( Rat2_score);
		 //System.out.println( Rat3_score);
		 //.out.println(SelectedRat) ;
		 
		 return SelectedRat;
		 
		
		 //return SelectedRat;
		 
	}
	
	int swap (int tempo){
		return tempo;
	}
	

}
